class Validello {
	constructor(schema) {
		let errorFunction;
		let selected;
	}
	validate() {
		if (this.schema) {
			this.addInputs();
			this.addValues();
			for (let key in this.schema) {
				//If the element type do not exist, we are going to set automatically to text
				if (!this.schema[key].type) this.schema[key].type = 'text';
				if (this.schema[key].filters) {
					for (let i = 0; i < this.schema[key].filters.length; i++) {
						let elem = this.schema[key];
						//This will be usefull to use in case of errors!
						elem.name = key;
						let func = elem.filters[i].function;
						let args = elem.filters[i].args;
						try {
							let result = func(args, elem.value, elem);
						} catch (error) {
							if (error) {
								if (!elem.errorMessage) {
									elem.errorMessage = error.message;
								}
								this.throwError(elem);
							}
							return false;
						}
					}
				}
			}
		} else {
			throw new Error('No Schema for this Validation');
		}
		return true;
	}
	addInputs() {
		this.selected = document.querySelectorAll('[_v]');
		if (this.selected.length == 0) {
			let message = '0 Validello Attributes (_v) founded';
			console.log(message);
			throw new Error(message);
		}

		this.selected.forEach((input) => {
			let inputName = input.getAttribute('_v');
			if (inputName && inputName in this.schema) {
				let elem = this.schema[inputName];
				elem.input = input;
			}
		});
	}
	addValues() {
		if (this.selected) {
			for (let key in this.schema) {
				let elem = this.schema[key];
				switch (elem.type) {
					case 'radio':
						for (let i = 0; i < this.selected.length; i++) {
							const input = this.selected[i];
							if (input.getAttribute('_v') == key && input.checked) {
								elem.value = input.value;
							}
						}
						break;
					case 'select':
						if (elem.input) {
							elem.value = elem.input.options[elem.input.selectedIndex].value;
						}
						break;
					default:
						if (elem.input) {
							elem.value = elem.input.value;
						}
						break;
				}
			}
		}
	}
	throwError(elem) {
		if (elem.error && typeof (elem.error == Function)) {
			elem.error(elem);
		}
		if (!elem.preventDefault && this.errorFunction) {
			this.errorFunction(elem);
		}
	}

	addSchema(schema) {
		if (schema) {
			this.schema = schema;
		} else {
			throw new Error('No schema added');
		}
	}
	addDefaultError(errorFunction) {
		this.errorFunction = errorFunction;
	}

	required(...a) {
		return {
			args: a,
			function: (args, value) => {
				if (!value) throw new Error('La valeur est démandé');
				return true;
			}
		};
	}
	alphanum(...args) {
		return {
			args: args,
			function: (args, value) => {
				if (value) {
					let re = /^[a-zA-Z0-9]*$/;
					return re.exec(value);
				} else {
					return true;
				}
			}
		};
	}
	email(...args) {
		return {
			args: args,
			function: (args, value) => {
				if (value) {
					let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
					if (!re.exec(value)) {
						throw new Error("Ceci n'est pas un mail valide");
					}
				} else {
					return true;
				}
			}
		};
	}
	max(...args) {
		return {
			args: args,
			function: (args, value) => {
				if (value) {
					let arg = args[0];
					if (!isNaN(value) && value <= arg) {
						return true;
					} else {
						throw new Error('LIMITED TO ' + arg);
					}
				} else {
					return true;
				}
			}
		};
	}
	siret(...args) {
		return {
			args: args,
			function: (args, value) => {
				if (value) {
					let re = /^[0-9]{12}$/;
					if (!re.exec(value)) {
						throw new Error('Numero de SIRET pas correct');
					}
				} else {
					return true;
				}
			}
		};
	}
	sameAs(...args) {
		return {
			args: args,
			function: (args, value, elem) => {
				console.log('same as ->', args, value);
				let reference = args[0];
				if (this.schema) {
					if (this.schema[reference]) {
						if (this.schema[reference].value != value) {
							throw new Error(
								"La Valeur de l'element " +
									elem.name +
									" est differente de la valeur de l'element " +
									reference
							);
						}
					} else {
						throw new Error(reference + " n'existe pas");
					}
				}
			}
		};
	}
	number(...args) {}
}
